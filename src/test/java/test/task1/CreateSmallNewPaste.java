package test.task1;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.ResultPage;
import test.BeforeAll;

public class CreateSmallNewPaste extends BeforeAll {
    // test data
    private String code = "Hello from WebDriver";
    private String expirationPeriod = "10 Minutes";
    private String title = "helloweb";

    @Test
    public void testCreateNewPaste() {
        ResultPage resultPage = mainPage
                .openPastebinPage()
                .fillTextArea(code)
                .clickExpirationArrow()
                .selectExpirationPeriod(expirationPeriod)
                .fillTitle(title)
                .clickSubmitButton();

        Assert.assertTrue(resultPage.isPasteCreatedSuccessfully(), "Failed to create a new paste.");
    }
}
