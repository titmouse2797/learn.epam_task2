package test.task2;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.ResultPage;
import test.BeforeAll;

public class CreateExtendedNewPasteTest extends BeforeAll {
    // test data
    private String code = " git config --global user.name  \"New Sheriff in Town\"\n" +
            "            git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
            "            git push origin master --force";
    private String syntax = "Bash";
    private String expirationPeriod = "10 Minutes";
    private String title = "how to gain dominance among developers";

    @Test
    public void createExtendedNewPasteTitleTest() {
        ResultPage resultPage = createExtendedPaste();
        Assert.assertTrue(resultPage.getResultPageTitle().contains(title),
                "Page title does not match the expected title.");
    }

    @Test
    public void createExtendedNewPasteSyntaxTest() {
        ResultPage resultPage = createExtendedPaste();
        Assert.assertEquals(resultPage.getSyntaxText(), syntax,
                "Selected syntax is not displayed as expected.");
    }

    @Test
    public void createExtendedNewPasteCodeTest() {
        ResultPage resultPage = createExtendedPaste();
        Assert.assertTrue(resultPage.getCodeText().contains(code),
                "Entered code does not match the expected code.");
    }

    // could be moved in before or services in future
    private ResultPage createExtendedPaste() {
        return mainPage
                .openPastebinPage()
                .fillTextArea(code)
                .clickSyntaxArrow()
                .selectPopularSyntax(syntax)
                .clickExpirationArrow()
                .selectExpirationPeriod(expirationPeriod)
                .fillTitle(title)
                .clickSubmitButton();
    }
}