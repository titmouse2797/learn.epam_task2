package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import page.MainPage;

public class BeforeAll {
    protected WebDriver driver;
    protected MainPage mainPage;

    @BeforeMethod
    public void setUp() {
        driver = new ChromeDriver();
        mainPage = new MainPage(driver);
    }

    @AfterMethod
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}


