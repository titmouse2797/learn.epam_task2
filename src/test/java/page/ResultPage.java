package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ResultPage {
    private WebDriver driver;
    private WebDriverWait wait;
    private By successMessageLocatorText = By.xpath("//div[contains(@class, 'success')]");
    private By selectedSyntaxLocator = By.xpath("//a[contains(@href,'/archive/')]");
    private By codeTextAreaLocator = By.xpath("//div[contains(@class,'source')]");

    public ResultPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        wait.until(ExpectedConditions.presenceOfElementLocated(codeTextAreaLocator));
    }

    public boolean isPasteCreatedSuccessfully() {
        WebElement successMessageText = wait.until(ExpectedConditions.presenceOfElementLocated(successMessageLocatorText));
        return successMessageText.isDisplayed();
    }

    public String getResultPageTitle() {
        return driver.getTitle();
    }

    public String getSyntaxText() {
        return driver.findElement(selectedSyntaxLocator).getText();
    }

    public String getCodeText() {
        return driver.findElement(codeTextAreaLocator).getText();
    }
}